FROM ruby:2.4

RUN gem install bundler --no-ri --no-rdoc

RUN mkdir /gem
WORKDIR /gem

ADD lib/rspec/roda/version.rb /gem/lib/rspec/roda/version.rb
ADD *.gemspec /gem/
ADD Gemfile /gem/Gemfile
RUN bundle install -j $(nproc) --path=/vendor/bundle
