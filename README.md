# Rspec::Roda

RSpec matchers for Roda apps and plugins.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rspec-roda'
```

And then execute:

```bash
bundle
```

Or install it yourself as:

```bash
gem install rspec-roda
```

## Usage

With Roda app:

```ruby
# app.rb
class App < Roda
  route do |r|
    r.on 'hello' do
      'Hey!'
    end
  end
end

# spec/spec_helper.rb
require 'rspec-roda'

# spec/app_spec.rb
RSpec.describe App, roda: :app do
  describe '/hello' do
    before { get '/hello' }

    it { is_expected.to be_successful }
    its(:body) { is_expected.to eq 'Hey!' }
  end
end 
```

With Roda plugin:

```ruby
# lib/roda/plugins/some.rb
class Roda
  module RodaPlugins
    module Some
      # ...
    end

    register_plugin :some, Some
  end
end

# spec/spec_helper.rb
require 'rspec-roda'

# spec/roda/plugins/some_spec.rb
RSpec.describe Roda::RodaPlugins::Some, roda: :plugin do
  configure do
    plugin :some # you don’t have to specify plugin itself in block with 
                 # `roda: :plugin` metadata, it will be assumed by `rspec-roda`.
  end

  specify 'roda-rspec lets following', :aggregate_failures do
    expect(roda_class).to be_a(Class)  
    expect(roda_class).to be < Roda  

    expect(app).to be_a(Roda)  
    expect(app.class).to be eq roda_class  
  end
end 
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bin/rake install`. To release a new version, update the version number in `version.rb`, and then run `bin/rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/alsemyonov/rspec-roda. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

© [Alex Semyonov](https://alex.semyonov.us/), <[alex@semyonov.us](mailto:alex@semyonov.us?subject=rspec-roda)> 2017
