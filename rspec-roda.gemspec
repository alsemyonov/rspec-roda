# frozen_string_literal: true

require_relative 'lib/rspec/roda/version'

Gem::Specification.new do |spec|
  spec.name = 'rspec-roda'
  spec.version = RSpec::Roda::VERSION
  spec.authors = ['Alex Semyonov']
  spec.email = ['alex@semyonov.us']

  spec.summary = 'RSpec and Roda integration'
  spec.description = 'RSpec helpers for testing Roda applications and plugins'
  spec.homepage = 'http://alsemyonov.gitlab.com/rspec-roda/'
  spec.license = 'MIT'

  files = `git ls-files -z`.split("\x0")
  spec.files = files.reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.test_files = files - spec.files
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'dry-inflector'
  spec.add_runtime_dependency 'rack-test', '~> 1.0'
  spec.add_runtime_dependency 'roda'
  spec.add_runtime_dependency 'rspec', '~> 3.7'
  spec.add_runtime_dependency 'rspec-its', '~> 1.2.0'

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'bundler-audit', '~> 0.6.0'
  spec.add_development_dependency 'rake', '~> 12.0'
  spec.add_development_dependency 'rubocop', '~> 0.57'
  spec.add_development_dependency 'simplecov', '~> 0.16'
  spec.add_development_dependency 'yard', '~> 0.9.9'
end
