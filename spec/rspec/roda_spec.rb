# frozen_string_literal: true
require 'spec_helper'

describe RSpec::Roda do
  it 'has a version number' do
    expect(RSpec::Roda::VERSION).not_to be nil
  end
end
