# frozen_string_literal: true

module RSpec
  module Roda
    VERSION = '0.2.2'
  end
end
